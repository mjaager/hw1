public class ColorSort {

   enum Color {red, green, blue};

   public static void main (String[] param) {
      // for debugging
   }

   public static void reorder (Color[] balls) {
      int r = 0;
      int g = 0;
      int b = 0;
      int where = 0;

      for (int i = 0; i < balls.length; i++) {
         if (balls[i].equals(Color.red)){
            r++;
         } else if (balls[i].equals(Color.green)){
            g++;
         } else if (balls[i].equals(Color.blue)){
            b++;
         }else {
            return;
         }
      }

      for (int i = 0; i < r; i++) {
         balls[where] = Color.red;
         where ++;
      }

      for (int i = 0; i < g; i++) {
         balls[where] = Color.green;
         where ++;
      }

      for (int i = 0; i < b; i++) {
         balls[where] = Color.blue;
         where ++;
      }
   }
}

